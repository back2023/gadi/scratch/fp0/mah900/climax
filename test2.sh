i=0
HOST=`hostname`
RANK=-1
while read line; do
   #echo -ne "${i}. "  
   #echo $line  
   if [[ "$line" == "$HOST" ]]; then
	   RANK=$i
   fi	   
   i=$((i+1))
done < /g/data/wb00/admin/staging/ClimaX/cls_nodes 
#echo $HOST
#echo $RANK
export NODE_RANK=${RANK}
export WORLD_SIZE=8 
export MASTER_ADDR=$(head -n 1 /g/data/wb00/admin/staging/ClimaX/cls_nodes)
export MASTER_PORT=10001

echo "${NODE_RANK} ${WORLD_SIZE} ${MASTER_ADDR}  ${MASTER_PORT}"
