echo "PBS_NODEFILES ${PBS_NODEFILE}"
N=$(cat $PBS_NODEFILE | uniq) 
echo $N
N_LIST=''
while IFS= read -r line ; do
	N_LIST+="${line::-16}:1,"
done < <(cat $PBS_NODEFILE | uniq) 
#done <<< "$N"

N_LIST=${N_LIST::-1}
echo "N_LIST: ${N_LIST}"

module load openmpi
mpiexec -np 2 -H ${N_LIST}  --display-allocation hostname 
