# Copyright (c) Microsoft Corporation.
# Licensed under the MIT license.

import os

from climax.regional_forecast.datamodule import RegionalForecastDataModule
from climax.regional_forecast.module import RegionalForecastModule
from pytorch_lightning.cli import LightningCLI
import torch


def main():
    # Initialize Lightning with the model and data modules, and instruct it to parse the config yml
    cli = LightningCLI(
        model_class=RegionalForecastModule,
        datamodule_class=RegionalForecastDataModule,
        seed_everything_default=42,
        save_config_overwrite=True,
        run=False,
        auto_registry=True,
        parser_kwargs={"parser_mode": "omegaconf", "error_handler": None},
    ) 
    
  
    os.makedirs(cli.trainer.default_root_dir, exist_ok=True)

    cli.datamodule.set_patch_size(cli.model.get_patch_size())

    normalization = cli.datamodule.output_transforms
    mean_norm, std_norm = normalization.mean, normalization.std
    mean_denorm, std_denorm = -mean_norm / std_norm, 1 / std_norm
    cli.model.set_denormalization(mean_denorm, std_denorm)
    cli.model.set_lat_lon(*cli.datamodule.get_lat_lon())
    cli.model.set_pred_range(cli.datamodule.hparams.predict_range)
    cli.model.set_val_clim(cli.datamodule.val_clim)
    cli.model.set_test_clim(cli.datamodule.test_clim)

    # 2023.02.21
    from pytorch_lightning import LightningModule
    from climax.regional_forecast.arch import RegionalClimaX
    #model = RegionalForecastModule(net=RegionalClimaX)
    #, pretrained_path="/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt")
    #cli.model = LightningModule.load_from_checkpoint("/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt")
    #cli.model.load_from_checkpoint("/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt")

    
    #cli.trainer.fit(cli.model, datamodule=cli.datamodule, ckpt_path="/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt")

 
    #model.set_denormalization(mean_denorm, std_denorm)
    #model.set_lat_lon(*cli.datamodule.get_lat_lon())
    #model.set_pred_range(cli.datamodule.hparams.predict_range)
    #model.set_val_clim(cli.datamodule.val_clim)
    #model.set_test_clim(cli.datamodule.test_clim)   

    #model = LightningModule.load_from_checkpoint("/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt")

    # fit() runs the training
    cli.trainer.fit(cli.model, datamodule=cli.datamodule)
    x, y, predictions = cli.trainer.predict(cli.model, datamodule=cli.datamodule, return_predictions=True)
    # test the trained model
    #cli.trainer.test(cli.model, datamodule=cli.datamodule, ckpt_path="best")

    #print (type(predictions))
    #print (type(predictions[0]))
    path='/g/data/wb00/admin/staging/climax_train_regional/test'
    torch.save(predictions, path+ '/preds-3.pt')
    torch.save(x,path+ '/x-3.pt')
    torch.save(y,path+ '/y-3.pt')
    ####################################



if __name__ == "__main__":
    main()
